from django.shortcuts import render
from .serializers import UserSerializer
from rest_framework_jwt.settings import api_settings
from rest_framework import permissions, status, generics, filters
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser, MultiPartParser
from django.contrib.auth import get_user_model, authenticate, login
from .serializers import (
    TransactionsSerializer
)
from .models import (
    Account,
    Transactions,
)
from django.db import transaction
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from .tasks import task_transfer

# Create your views here.
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()


class AddUserView(generics.CreateAPIView):
    # authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)
    queryset = User.objects.all()


class ListUserView(generics.ListAPIView):
    # authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    # authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()


class TransferView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        value = request.data.get("value", None)
        receiver = request.data.get("receiver", None)
        note = request.data.get("note", None)

        if receiver == self.request.user.pk:
            return Response(data='You can not transfer to yourself', status=status.HTTP_400_BAD_REQUEST)

        sender = self.request.user.pk

        # save transaction
        transaction_serializer = TransactionsSerializer(data={
            'sender' : sender,
            'receiver' : receiver,
            'value' : value,
            'note' : note,
            'status' : '1'
        })

        if transaction_serializer.is_valid():
            transaction = transaction_serializer.save()

            task_transfer.delay({
                'sender' : sender,
                'receiver' : receiver,
                'value' : value,
                'transaction_id': transaction.pk
            })

            return Response(data=transaction_serializer.data)
        else:
            return Response(data=str(transaction_serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    
    @transaction.atomic
    def handle_transfer(self, payload):
        if not 'sender' in payload:
            raise ValidationError('sender is required')
        if not 'receiver' in payload:
            raise ValidationError('receiver is required')
        if not 'value' in payload:
            raise ValidationError('value is required')

        if 'fee' in payload:
            fee = float(payload['fee'])
        else:
            fee = 0

        if 'note' in payload:
            note = payload['note']
        else:
            note = None
        
        sender = payload['sender']
        receiver = payload['receiver']
        value = float(payload['value'])

        # subtract sender account
        sender_account = Account.objects.filter(user_id=sender)[:1][0]
        if sender_account.balance >= (value + fee):
            sender_account.balance -= (value + fee)
            sender_account.save()
        else:
            return Response(data='sender account is not affortdable to this transaction', status=status.HTTP_400_BAD_REQUEST)

        # add receiver account
        receiver_account = Account.objects.filter(user_id=receiver)[:1][0]
        receiver_account.balance += value
        receiver_account.save()

        # save transaction
        transaction_serializer = TransactionsSerializer(data={
            'sender' : sender,
            'receiver' : receiver,
            'value' : value,
            'note' : note
        })

        if transaction_serializer.is_valid():
            transaction = transaction_serializer.save()

            return Response(data=transaction_serializer.data)
        else:
            return Response(data=str(transaction_serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class BalanceView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            account = Account.objects.filter(user=self.request.user)[:1][0]
            return Response(data={
                "balance" : account.balance
            })
        except Exception as e:
            return Response(data=str(e), status=status.HTTP_400_BAD_REQUEST)


class DepositView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        value = request.data.get("value", None)
        try:
            account = Account.objects.filter(user=self.request.user)[:1][0]
            account.balance += float(value)
            account.save()

            transaction_serializer = TransactionsSerializer(data={
                'receiver' : self.request.user.pk,
                'value' : value,
                'note' : self.request.user.username + ' deposit ' + str(value)
            })

            if transaction_serializer.is_valid():
                transaction = transaction_serializer.save()

                return Response(data=transaction_serializer.data)
            else:
                return Response(data=str(transaction_serializer.errors), status=status.HTTP_400_BAD_REQUEST)

            return Response(data={
                "balance" : account.balance
            })
        except Exception as e:
            return Response(data=str(e), status=status.HTTP_400_BAD_REQUEST)


class TransactionView(generics.ListAPIView):
    # authentication_classes = (CsrfExemptSessionAuthentication,)
    serializer_class = TransactionsSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Transactions.objects.all()