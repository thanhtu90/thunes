from django.urls import path
from .views import (
    AddUserView,
    ListUserView,
    UserDetailView,
    TransferView,
    TransactionView,
    BalanceView,
    DepositView
)

urlpatterns = [
    # User
    path('register', AddUserView.as_view(), name="register"),
    path('user', ListUserView.as_view(), name="user-all"),
    path('user/<int:pk>', UserDetailView.as_view(), name="user-detail"),
    # Transaction
    path('transfer', TransferView.as_view(), name="transfer"),
    path('transaction', TransactionView.as_view(), name="transaction"),
    # Balance
    path('balance', BalanceView.as_view(), name="balance"),
    # Deposit
    path('deposit', DepositView.as_view(), name="deposit"),
]