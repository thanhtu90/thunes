from __future__ import absolute_import, unicode_literals
from datetime import date
from celery import task
import calendar
import sys
from django.utils import timezone
from .serializers import (
    TransactionsSerializer
)
from .models import (
    Account,
    Transactions,
    TRANSACTION_STATUS
)
from django.db import transaction
from django.db import IntegrityError

@task()
def task_transfer(payload):
    if not 'sender' in payload:
        raise ValidationError('sender is required')
    if not 'receiver' in payload:
        raise ValidationError('receiver is required')
    if not 'value' in payload:
        raise ValidationError('value is required')
    if not 'transaction_id' in payload:
        raise ValidationError('transaction_id is required')

    transaction = Transactions.objects.filter(id=payload['transaction_id'])[:1][0]

    try:
        handle_transfer(payload)

        COMPLETED_STATUS = TRANSACTION_STATUS[2][0]
        transaction.status = COMPLETED_STATUS
        transaction.save() 

    except Exception as e:
        # update transaction status + rejected_reason
        REJECTED_STATUS = TRANSACTION_STATUS[1][0]
        transaction.status = REJECTED_STATUS
        transaction.rejected_reason = str(e)
        transaction.save()


@transaction.atomic
def handle_transfer(payload):
    if 'fee' in payload:
        fee = float(payload['fee'])
    else:
        fee = 0

    if 'note' in payload:
        note = payload['note']
    else:
        note = None
    
    sender = payload['sender']
    receiver = payload['receiver']
    value = float(payload['value'])

    # subtract sender account
    sender_account = Account.objects.filter(user_id=sender)[:1][0]
    if sender_account.balance >= (value + fee):
        sender_account.balance -= (value + fee)
        sender_account.save()
    else:
        raise IntegrityError('sender account is not affortdable to this transaction')

    # add receiver account
    receiver_account = Account.objects.filter(user_id=receiver)[:1][0]
    receiver_account.balance += value
    receiver_account.save()

    return True