# Generated by Django 3.0.6 on 2020-05-28 09:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fund_transfer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactions',
            name='fee',
            field=models.FloatField(blank=True, default=0.0, null=True),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='note',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
