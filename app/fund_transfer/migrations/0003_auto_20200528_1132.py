# Generated by Django 3.0.6 on 2020-05-28 11:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fund_transfer', '0002_auto_20200528_0948'),
    ]

    operations = [
        migrations.AddField(
            model_name='transactions',
            name='rejected_reason',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='transactions',
            name='status',
            field=models.CharField(choices=[('1', 'Pending'), ('2', 'Rejected'), ('3', 'Completed')], default='1', max_length=2),
        ),
    ]
