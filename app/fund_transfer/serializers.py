from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from .models import (
    Account,
    Movement,
    Transactions
)
import logging
from datetime import datetime

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "last_login",
            "is_superuser",
            "username",
            "first_name",
            "last_name",
            'password',
            "email",
            "is_staff",
            "is_active",
            "date_joined",
            "groups",
            "user_permissions",
        )
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email= validated_data['email'] if 'email' in validated_data else '',
            username=validated_data['username'],
            is_staff=validated_data['is_staff']
        )
        user.set_password(validated_data['password'])
        user.save()

        # create associated account
        account = Account(
            user=user,
            balance=0
        )
        account.save()

        # create associated movement
        movement = Movement(
            user=user,
            year=datetime.now().year,
            month=datetime.now().month,
            opening_balance=0
        )
        return user


class TransactionsSerializer(ModelSerializer):
    class Meta:
        model = Transactions
        exclude = ()