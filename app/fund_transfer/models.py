from django.db import models
from django.contrib.auth.models import User

TRANSACTION_STATUS = (
    ('1', "Pending"),
    ('2', "Rejected"),
    ('3', "Completed")
)

# Create your models here.
class Account(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    balance = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Transactions(models.Model):
    sender = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True, related_name="transaction_sender",)
    receiver = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True, related_name="transaction_receiver",)
    note = models.CharField(max_length=255, null=True, blank=True)
    fee_payer = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True, related_name="transaction_fee_payer",)
    value = models.FloatField()
    fee = models.FloatField(null=True, blank=True, default=0.0)
    status = models.CharField(
        max_length=2, choices=TRANSACTION_STATUS, default='1')
    rejected_reason = models.CharField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Movement(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    year = models.IntegerField()
    month = models.IntegerField()
    opening_balance = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)